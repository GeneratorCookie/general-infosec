# Introduction

## Summary

This repository lays down examples/instances of infosec breaches and vulnerabilities either from private corporations or public institutions. It provides examples that can be understood by the layman to make them understand they should care about their online presence.

## Motivation

I've had trouble explaining with data to close friends/family why they should care about their online privacy, and I decided to compile a list of examples and summarize them in a way a layperson can understand. Hopefully this document can raise awareness.

# Companies Selling Your Data

## Avast antivirus selling browsing history & location data

> The documents, from a subsidiary of the antivirus giant Avast called Jumpshot, shine new light on the secretive sale and supply chain of peoples' internet browsing histories. They show that the Avast antivirus program installed on a person's computer collects data, and that Jumpshot repackages it into various different products that are then sold to many of the largest companies in the world. Some past, present, and potential clients include Google, Yelp, Microsoft, McKinsey, Pepsi, Sephora, Home Depot, Condé Nast, Intuit, and many others. Some clients paid millions of dollars for products that include a so-called "All Clicks Feed," which can track user behavior, clicks, and movement across websites in highly precise detail.

From [here](https://www.vice.com/en_us/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation)

> The data obtained by Motherboard and PCMag includes Google searches, lookups of locations and GPS coordinates on Google Maps, people visiting companies' LinkedIn pages, particular YouTube videos, and people visiting porn websites. It is possible to determine from the collected data what date and time the anonymized user visited YouPorn and PornHub, and in some cases what search term they entered into the porn site and which specific video they watched.

From [here](https://www.vice.com/en_us/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation)

# Breaches & Vulnerabilities

## Private Corporations

### Ring employees watching private videos of their customers

1. https://www.vice.com/en_us/article/y3mdvk/ring-fired-employees-abusing-video-data

### Ring shares camera installation heatmap to law enforcement

1. https://www.engadget.com/2019-12-03-amazon-ring-video-doorbell-police-map.html

### Facebook employees payroll information stolen from unencrypted hard drives 

1. https://www.bloomberg.com/news/articles/2019-12-13/thief-stole-payroll-data-for-thousands-of-facebook-employees

### Facebook storing unencrypted passwords on their servers

> `[...]` between 200 million and 600 million Facebook users may have had their account passwords stored in plain text and searchable by more than 20,000 Facebook employees.

From [here](https://krebsonsecurity.com/2019/03/facebook-stored-hundreds-of-millions-of-user-passwords-in-plain-text-for-years/)

> `[...]` access logs showed some 2,000 engineers or developers made approximately nine million internal queries for data elements that contained plain text user passwords.

From [here](https://krebsonsecurity.com/2019/03/facebook-stored-hundreds-of-millions-of-user-passwords-in-plain-text-for-years/)

The official statement from Facebook about this incident can be found [here](https://about.fb.com/news/2019/03/keeping-passwords-secure/)

Date: March 2019

Summary:
1. Facebook was logging the unencrypted password of some of its users on its servers
1. Those logs are stored for a certain amount of time on their machine for operational reasons, which allowed its engineers to have access to this information
1. It does not look like those passwords were stored for authentication purposes, but as a result of bad logging practices followed by the company

Implications:
1. Any Facebook employee having access to those servers was able to steel the passwords of the affected users and either leak it or use it

### Twitter storing unencrypted passwords on their servers

### Facebook uses 2FA phone number for advertising and public search



## Public Institutions


## Technology

### HTTPS

1. snooping traffic, injecting malicious code into the client's browser
1. give the example of comcast injecting content into my browser
1. give the example of the chinese government ddos github by injecting malicious code

### Torrent

1. public tables, everyone has access to what is shared and who wants to access it